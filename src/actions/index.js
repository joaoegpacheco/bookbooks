import { 
  LOGIN_AUTHENTICATION,
  USER_DATAS,
  ADD_BOOKS,
  ADD_READED,
  ADD_WISHLIST,
  ADD_WISHLIST_BOOK,
  ADD_READING_BOOK,
  ADD_READED_BOOK,
  ADD_READING,
  DELETE_BOOK_WISHLIST,
  DELETE_BOOK_READING,
  UPDATE_BOOK_REVIEW,
  CLEAR_BOOKS
} from "./actionTypes";

export const login = (authentication) => ({
  type: LOGIN_AUTHENTICATION,
  authentication,
});

export const userDatas = (user) => ({
  type: USER_DATAS,
  user,
});

export const addBooks = (books) => ({
  type: ADD_BOOKS,
  books,
});
export const addReaded = (readed) => ({
  type: ADD_READED,
  readed,
});
export const addWishlist = (wishlist) => ({
  type: ADD_WISHLIST,
  wishlist,
});
export const addWishlistBook = (book) => ({
  type: ADD_WISHLIST_BOOK,
  book,
});
export const addReadingBook = (book) => ({
  type: ADD_READING_BOOK,
  book,
});
export const addReadedBook = (book) => ({
  type: ADD_READED_BOOK,
  book,
});
export const addReading = (reading) => ({
  type: ADD_READING,
  reading,
});

export const deleteWishlist = (key) => ({
  type: DELETE_BOOK_WISHLIST,
  key,
});

export const deleteReading = (key) => ({
  type: DELETE_BOOK_READING,
  key,
});

export const updateBookReview = (key, review, grade) => ({
  type: UPDATE_BOOK_REVIEW,
  key,
  review,
  grade,
});

export const clearBooks = () => ({
  type: CLEAR_BOOKS,
});
