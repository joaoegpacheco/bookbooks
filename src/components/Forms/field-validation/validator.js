const errorRules = {
  name: [
    {
      validator: (rule, value) => {
        if (/[^áéía-zA-Z\s]/gi.test(value)) {
          return Promise.reject("Não pode conter caracteres especiais.");
        }
        return Promise.resolve();
      },
    },

    { required: true, message: "Por favor, preencha o campo nome." },
  ],

  user: [
    {
      validator: (rule, value) => {
        if (/[^a-zA-Z]+/gi.test(value)) {
          return Promise.reject("Apenas caracteres alfabéticos");
        }
        return Promise.resolve();
      },
    },

    { required: true, message: "Por favor, preencha o campo usuário." },
  ],

  email: [
    {
      type: "email",
      message: "Email inválido.",
    },
    {
      required: true,
      message: "Por favor, preencha seu email.",
    },
  ],

  password: [
    {
      required: true,
      message: "Por favor, preencha o campo senha.",
    },
  ],
};

export default errorRules;
