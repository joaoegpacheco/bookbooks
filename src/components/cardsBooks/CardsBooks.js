import React from "react";
import "./CardBooks.css";
import { Card, Button, notification } from "antd";
import { connect } from "react-redux";
import { addWishlist } from "../../actions";
import { withRouter } from "react-router-dom";

class CardBooks extends React.Component {
  addToWishlist = (book) => {
    const idUser = JSON.parse(localStorage.getItem("user")).id;
    const urlApi = `https://ka-users-api.herokuapp.com/users/${idUser}/books`;
    const data = {
      book: {
        title: book.volumeInfo.title,
        author: book.volumeInfo.authors
          ? book.volumeInfo.authors[0]
          : "Sem autor",
        shelf: 1,
        image_url: book.volumeInfo.imageLinks
          ? book.volumeInfo.imageLinks.thumbnail
          : "https://i.pinimg.com/originals/b4/9e/7a/b49e7a7298b855f8bf2cd3f5923ea7ab.jpg",
        grade: 1,
        review: "Este livro ainda não tem uma avaliação",
        google_book_id: book.id,
      },
    };

    fetch(urlApi, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: localStorage.getItem("currentToken"),
      },
      body: JSON.stringify(data),
    }).then((res) => {
      if (res.ok) {
        return notification.success(
          {
            message: book.volumeInfo.title,
            description: "Adicionado à sua lista de leituras",
          },
          4
        );
      }
    });
  };

  render() {
    return (
      <div className="Container">
        <div className="CardsMother">
          {this.props.booksMap.map((book, key) => {
            return (
              <Card
                loading={false}
                hoverable
                style={{ width: 458, height: 180, marginTop: 20 }}
                key={key}
              >
                <Card.Grid
                  hoverable={false}
                  style={{
                    width: 136,
                    height: 180,
                    padding: 0,
                  }}
                >
                  <Card style={{ overflow: "hidden" }}>
                    <img
                      style={{ height: 180 }}
                      alt="capa do livro"
                      src={
                        book.volumeInfo.imageLinks
                          ? book.volumeInfo.imageLinks.thumbnail
                          : "https://i.pinimg.com/originals/b4/9e/7a/b49e7a7298b855f8bf2cd3f5923ea7ab.jpg"
                      }
                    />
                  </Card>
                </Card.Grid>
                <Card.Grid
                  hoverable={false}
                  style={{
                    width: 321,
                    height: 180,
                    padding: 0,
                  }}
                >
                  <Card
                    style={{
                      height: "100%",
                    }}
                  >
                    <Card.Meta
                      style={{ margin: 14 }}
                      title={book.volumeInfo.title}
                      description={
                        book.volumeInfo.description
                          ? book.volumeInfo.description.substring(0, 80)
                          : (book.volumeInfo.description = "")
                      }
                    />
                    <div
                      style={{
                        margin: 14,
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      {book.volumeInfo.authors ? (
                        <span>
                          <b>Autor:</b> {book.volumeInfo.authors[0]}
                        </span>
                      ) : (
                        <span>Sem autor</span>
                      )}

                      <Button
                        type="primary"
                        style={{
                          margin: 14,
                          display: "flex",
                          alignItems: "center",
                        }}
                        onClick={() => this.addToWishlist(book)}
                      >
                        Adicionar
                      </Button>
                    </div>
                  </Card>
                </Card.Grid>
              </Card>
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  books: state.books.books,
  state: state,
  bookWishlist: state.books.wishlist,
});

const mapDispatchToProps = (dispatch) => ({
  wishListDispatch: (whishlist) => dispatch(addWishlist(whishlist)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CardBooks));
